#!/usr/bin/perl

print "Content-type:text/html\n\n";

require 'jcode.pl';

#=======================設定はここから========================
# 管理者のメールアドレスを記入します
$mail_address = 'ootemachisasuke@yahoo.co.jp';

# Subject（件名）を記入
$pagename = 'お問い合わせ';

# sendmail部分のパスを記入
$send = '/usr/sbin/sendmail';  

#========================ここまで==============================

# 送信後に表示するHTMLファイル
$file01 = './send.html';

# エラー表示するHTMLファイル
$file02 = './error.html';

# エラー表示判断格納ファイル
$err_file = './error.txt';

# メール送信内容に添付するコメント記述ファイル
$mail_file = './mail.txt';

# 送信内容格納ファイル
$CSV = './data/data.cgi';

# 送信できないようにするURL又はメール記述ファイル
$stop_f = './stop_domain.cgi';

#-----------------------------------------------------

#$d_ref = $ENV{'HTTP_REFERER'};
#if($d_ref !~ /$baseurl/){ $mes = '所定の投稿フォーム以外の入力は認められません。';}
#if($mes ne ""){&error;}

#------------
if(! -f $stop_f){
	open(OUT,">$stop_f");
	close(OUT);
}

if(!open(IN,"$stop_f")){$mes = 'stop file open error';}
@stop = <IN>;
close(IN);
#------------
#-----------------------------------------------------
	if ($ENV{'REQUEST_METHOD'} eq "POST"){
	read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
	}else{
	$buffer = $ENV{'QUERY_STRING'};
	}
	@pairs = split(/&/,$buffer);
	foreach $pair (@pairs){
		($name, $value) = split(/=/, $pair);
		$value =~ tr/+/ /;
		$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
		$value =~ s/</&lt;/g;
		$value =~ s/>/&gt;/g;
		$value =~ s/"/&quot;/g;
		#$value =~ s/\r\n//g;
        $value =~ s/mailto//g;
        $value =~ s/file://g;
        $value =~ s/con//g;
        $value =~ s/prn//g;
        $value =~ s/nul//g;
        $value =~ s/aux//g;
        $value =~ s/\t//g;
        $value =~ s/java//ig;
        $value =~ s/script//ig;
        $value =~ s/meta//ig;
        $value =~ s/while//ig;
		$name =~ tr/+/ /;
		$name =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
		
		&jcode'convert(*name,'sjis');
		&jcode'convert(*value,'sjis');
		
		$form{$name} = $value;
#---------------------------------------------------------
		if($value =~ /(([\w\.\-\_]+)\@([\w\.\-]+))/){$MAIL = $value;}
		if($value eq "http://"){$mes = 'URLが誤っています。';}
		if($mes ne ""){&error;}
#---------------------------------------------------------
		$DATA = "$name\t$value\n";
#----特定のドメイン排除-----------------------------------
		foreach $STOPS (@stop){
			chomp $STOPS;
			if($DATA =~ /$STOPS/i){$mes = 'NO Send Mail';}
		}

		push(@NEW,$DATA);

	}

	if($buffer eq ""){ $mes = 'CGIへ直接アクセスすることはできません。';}
	if($mes ne ""){&error;}

	if(!open(IN,"$err_file")){ $mes = 'ファイルOPENエラーです。';}
	if($mes ne ""){&error;}
		@err = <IN>;
		close(IN);

	foreach $CK (@NEW){
		chomp $CK;
		($NAME,$VALUE) = split(/\t/,$CK);
		&jcode'convert(*NAME,'euc');

		foreach $ERR (@err){
			chomp $ERR;
			&jcode'convert(*ERR,'euc');
			if($ERR ne ""){
			if($ERR eq $NAME && $VALUE eq ""){
			&jcode'convert(*ERR,'sjis'); $MES .= qq|$ERRの項目が未入力です。<br>\n|;
			}
			$mes = $MES;
			}
		}
	}

	if($mes ne ""){&error;}

#--------送信ファイル内容の収得--------------------------------------

	if(!open(IN,"$mail_file")){$mes = 'メール用ファイルOPENエラーです。';}
	if($mes ne ""){&error;}
		@m_file = <IN>;
		close(IN);

	foreach $lines (@NEW){
		($A,$B) = split(/\t/,$lines);
		$ML_LINE = "$A ： $B\n";
		$ML_DATA .= $ML_LINE;
	}

	foreach $M_FILE (@m_file){
		#chomp $M_FILE;
		$M_FILE =~ s/##A##/$ML_DATA/g;
		$MAILDATA .= $M_FILE;
	}
#---------データベース化----------------------------------------------
	foreach $DATAS (@NEW){
		($dmy01,$dmy02) = split(/\t/,$DATAS);
		chomp $dmy02;
		$dmy02 =~ s/\r\n//g;
		push(@DATABASE,$dmy02);
	}

	$NEWDATA = join(',',@DATABASE);

	if(!open(OUT,">>$CSV")){$mes = 'ファイル書きこみエラーです。';}
	if($mes ne ""){&error;}
		print OUT "$NEWDATA\n";
		close(OUT);
#--------メール送信処理-----------------------------------------------
	$DMY = "$pagename\t$mail_address\t$MAILDATA\t$MAIL";
	&jcode'convert(*DMY,'jis');
	($subname,$mymail,$maildata,$tomail) = split(/\t/,$DMY);	

	if (open(OUT,"| $send $mymail")) {
		print OUT "To: $mymail\n";
		print OUT "From: $tomail\n";
		print OUT "Subject: $subname\n";
		print OUT "Content-Transfer-Encoding: 7bit\n";
		print OUT "Content-Type: text/plain\; charset=\"ISO-2022-JP\"\n\n";
#		print OUT "\n\n";
		print OUT "$maildata\n";
		close(OUT);

	}
#---------送信後の表示部分---------------------------------------------
	if(!open(IN,"$file01")){$mes = 'ファイルOPENエラーです。';}
	if($mes ne ""){&error;}
		@outfile = <IN>;
		close(IN);


	foreach $OUTFILES (@outfile){

		foreach $lines (@NEW){
			($A,$B) = split(/\t/,$lines);
			chomp $B;
			$B =~ s/\r\n/<br>/g;

			if($OUTFILES =~ /<!--A-->/){

#--------ここが送信後の表示部分タグです。表示内容に合わせて変更してください。----------
			print "<tr><th align=center width=15%>$A</th><td width=55%>$B</td></tr>\n";
#--------------------------------------------------------------------------------------

			}
		}
		print $OUTFILES;
	}
exit;
#-----------------------------------------------------------------------	
sub error{

	if(!open(IN,"$file02")){ print 'ファイルOPENエラーです。',"\n";}
		@erfile = <IN>;
		close(IN);

	foreach $ERFILES (@erfile){
		$ERFILES =~ s/##A##/$mes/g;
		print $ERFILES;
	}
	
exit;
}
