<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>新着情報・お知らせ一覧｜広島のエステサロン｜フルリール</title>
<meta name="keywords" content="エステ,エステサロン,フェイシャルエステ,ボディエステ,アロマエステ,ヘアメイク,着付け,ハンドスパ,広島" />
<meta name="description" content="広島の安芸郡海田町にあるプライベートサロンフルリール（Fleurir)です。フェイシャル・ボディ・アロマ・ヘアメイク・着付け・ハンドスパなどのコースがあります。マンツーマンでの施術です。" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="style.css">
<script type="text/javascript">
<!--
function openwin(url) {//PC用ポップアップ。ウインドウの幅、高さなど自由に編集できます
 wn = window.open(url, 'win','width=520,height=500,status=no,location=no,scrollbars=yes,directories=no,menubar=no,resizable=no,toolbar=no');wn.focus();
}
-->
</script>
</head>
<body>

<header>

<div id="page_top">
 <h1><center><a href="index.php"><img src="img/logo.jpg" style="width:100%; max-width:680px;" alt="エステサロン フルリール"></a></center></h1>
 <p style="text-align:center"><img src="img/header.jpg" style="width:100%; max-width:680px;" alt=""></p>
</div>

</header>

<article>

<section>
 <h2>新着情報・お知らせ一覧</h2>
<dl>
<?php
//----------------------------------------------------------------------
// 　任意設定箇所 (STSRT)　設置箇所によっては必要に応じて変更下さい
//----------------------------------------------------------------------

//設定ファイルインクルード（相対パス）※設置箇所が変わる場合は変更して下さい
include_once("../photo_news/config.php");
//データファイル（news.dat）の相対パス ※設置箇所が変わる場合は変更して下さい
$file_path = '../photo_news/data/news.dat';
//記事詳細ページの相対パス（このファイルから見たnews_post.phpのパス）
$post_path = 'news_post.php';
//このページの文字コード。Shift-jisは「SJIS」、EUC-JPは「EUC-JP」と指定。デフォルトはUTF-8。
$encodingType = 'UTF-8';

//----------------------------------------------------------------------
// 　任意設定箇所 (END)　設置箇所によっては必要に応じて変更下さい
//----------------------------------------------------------------------

//ファイルの内容を取得　表示
$lines = newsListSortUser(file($file_path),$encodingType);
$pager_res = pager_dsp($lines,$pagelength,$pagerDispLength,$encodingType);//ページャー生成
$pager_dsp = '<div class="pager_link">'.$pager_res['dsp'].'</div>';//ページャー用html
?>
<?php echo $pager_dsp;//ページャー表示?>
<ul id="news_list">
<?php
if(!$copyright){echo $warningMesse; exit;}else{
for($i = $pager_res['index']; ($i-$pager_res['index']) < $pagelength; $i++){
	if(!empty($lines[$i]) && strpos($lines[$i], 'no_disp')===false){
	$lines_array = explode(",",$lines[$i]);
	$upymd = ymd2format($lines_array[1],$encodingType);//日付フォーマットの適用
	$title = title_format($lines_array[3],$lines_array[2],$lines_array[0],$post_path);//タイトルのフォーマット
	$category = category_get($lines_array[4],$category_array,$encodingType);//カテゴリの設定チェック（カテゴリが設定されていなかったら非表示処理）
	
	//NEWマーク表示処理　※タグ部変更可。画像でももちOK（さらに下にある「{$new_mark}」を移動すれば表示場所を変えられます）
	if($new_mark = new_mark_func($lines_array[1],'<span style="color:red;font-size:12px;" class="new_mark"> NEW !</span>'));
		
//ブラウザ出力（編集可）
echo <<<EOF
<dt class="cat_{$lines_array[4]}"> <!-- カテゴリ毎にclassを付与（カテゴリ毎にCSSで背景画像などを指定可能） -->
<span class="cat_name"{$category['hidden_style']}>{$category['name']}</span> <!--　カテゴリ名表示（削除してCSSで背景画像等で指定してもOK） -->
<span class="news_List_Ymd">{$upymd}{$new_mark}</span></dt>
<dd><span class="news_List_Title">{$title}</span></dd>

EOF;
	}
}
?>
<?php echo $pager_dsp;//ページャー表示?>
<?php echo copyright_dsp($encodingType,$copyright); }//著作権表記削除不可?>
<dl>
<!--▲▲ 記事一覧表示 ▲▲-->
</section>

<section>
 <h2>フルリールのオススメ</h2>
 <div align="center"><img src="img/top_2.png" alt="おすすめのご案内" style="width:100%; max-width:300px;" /></div>
</section>

<section>
 <h2>ご予約につきまして</h2>
 <div align="center"><a href="http://www.ispot.jp/s/fleurir/" target="_blank"><img 
src="http://www.ispot.jp/img/info/banar_www_l.gif" alt="ispot 輝く女性を応援する
キレイ・健康・癒しの検索サイト" border="0"></a>　<a href="https://www.ispot.jp/s/fleurir/reserve/"
 target="_blank"><img
 src="http://www.ispot.jp/img/info/banar_reserve_l.gif" alt="ispot 
ネットで予約する" border="0"></a></div>
</section>

<section>
 <h2>リンク</h2>
 <div align="center"><a href="http://www.estessimo.jp/" target="_blank"><img src="img/spa.gif" alt="ESTESSiMO（エステシモ）" style="width:100%; max-width:300px;" /></a></div>
 <div align="center"><a href="http://www.estessimo.jp/" target="_blank">動画セルフハグマッサージはWEBで観覧できます。</a></div>
</section>

<section>
<h2>SNSにて紹介する</h2>
<div class="ninja_onebutton">
<script type="text/javascript">
//<![CDATA[
(function(d){
if(typeof(window.NINJA_CO_JP_ONETAG_BUTTON_44c9d352ed68705c72f5087254a9bc5a)=='undefined'){
    document.write("<sc"+"ript type='text\/javascript' src='http:\/\/omt.shinobi.jp\/b\/44c9d352ed68705c72f5087254a9bc5a'><\/sc"+"ript>");
}else{ 
    window.NINJA_CO_JP_ONETAG_BUTTON_44c9d352ed68705c72f5087254a9bc5a.ONETAGButton_Load();}
})(document);
//]]>
</script><span class="ninja_onebutton_hidden" style="display:none;"></span><span style="display:none;" class="ninja_onebutton_hidden"></span>
</div>
</section>

</article>

<nav>
 <h2>サイトメニュー</h2>
  <ul>
   <li><a href="index.php">ホーム</a></li>
   <li><a href="menu.html">メニュー</a></li>
   <li><a href="beautyequipment.html">美容機器</a></li>
   <li><a href="faq.html">よくあるご質問</a></li>
   <li><a href="access.html">サロン紹介</a></li>
   <li><a href="contact.html">お問い合わせ</a></li>
   <li><a href="voice.html">お客様の声</a></li>
   <li><a href="http://ameblo.jp/esthetic-fleurir/" target="_blank">ブログ</a></li>
  </ul>
</nav>

<div id="page_back">
 <p><a href="#page_top">▲ページトップに戻る</a></p>
</div>

<footer>
 <p><a href="http://www.esthetic-fleurir.com/index.html?vmode=pc">PCサイトへ</a> ｜ <a href="sitemap.html">サイトマップ</a></p>
 <p class="copy">Copyright (C) 2013 フルリール All Rights Reserved.</p>
</footer>

</body>
</html>