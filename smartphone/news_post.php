<!DOCTYPE HTML>
<html lang="ja">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5L3FD38');</script>
<!-- End Google Tag Manager -->
<meta charset="UTF-8">
<title>更新情報｜大手町Style整骨院</title>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="style.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="http://www.ootemachi-style.com/lightbox/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.ootemachi-style.com/lightbox/jquery.lightbox-0.5.css"/>
<script type="text/javascript">
//lightbox
$(function() {
	$(".detailPhoto a").lightBox();
});
</script>

<script type="text/javascript">
//本文内のaタグに_blankを付与 ※変更可
$(document).ready( function () {
	$("#detailWrap a").attr("target","_blank");
})
</script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5L3FD38"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header>

<div id="page_top">
 <h1><a href="../smartphone/"><img src="img/logo.png" alt="大手町Style整骨院" border="0" /></a></h1>
 <p style="text-align:center"><img src="img/main_1116.jpg" style="width:100%; max-width:950px;" alt=""></p>
</div>

</header>

<article>

<section>

<?php

//既存ページのデザインを反映する場合には必要な箇所のソースを以下のhtmlソース部にただコピペすればOKです
//HTML、CSS、JS部分などは自由に編集可能です。

//----------------------------------------------------------------------
// 設定ファイルの読み込みとページ独自設定　※必要に応じて変更下さい(START)
//----------------------------------------------------------------------

//設定ファイルインクルード
include_once("../photo_news/config.php");
//データファイル（news.dat）の相対パス ※設置箇所が変わる場合は変更して下さい
$file_path = '../photo_news/data/news.dat';
//画像の保存先を指定
$img_updir = "../photo_news/upimg";
//このページの文字コード。Shift-jisは「SJIS」、EUC-JPは「EUC-JP」と指定してください。デフォルトはUTF-8。
$encodingType = 'UTF-8';

//----------------------------------------------------------------------
// 設定ファイルの読み込みとページ独自設定　※必要に応じて変更下さい(END)
//----------------------------------------------------------------------

	if(!preg_match("/^[a-zA-Z0-9]+$/", $_GET['id']) || empty($_GET['id'] )) exit('パラメータが不正です');
	$id=$_GET['id'];
    $lines = newsListSortUser(file($file_path),$encodingType);
	foreach($lines as $key => $val){
	$lines_array = explode(",",$val);
	  if($lines_array[0] == $id){
		  $end_flag = 1;
		  break;
	  }
	}
	if($end_flag != 1) exit('データ取得エラー');
	$lines_array[5] = rtrim($lines_array[5]);
	$lines_array[1] = ymd2format($lines_array[1],$encodingType);//日付フォーマットの適用
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<title><?php	echo $lines_array[2];//記事のタイトルを表示?></title>
<meta name="Keywords" content="" />
<meta name="Description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<meta name="format-detection" content="telephone=no">

</head>
<body>

<!--▼▼ 記事表示部（html部などは編集可） ▼▼-->
<div id="wrap">
<?php
if(!$copyright){echo $warningMesse; exit;}else{
if(!empty($lines_array[5])){
	$caption_array = explode('<>',$lines_array[5]);
}
//ブラウザ出力（HTML部は編集可）
echo <<<EOF
<h2>{$lines_array[2]}</h2> <!-- タイトル表示 -->
<p class="up_ymd">{$lines_array[1]}</p> <!-- 日付表示 -->
<div id="detailWrap" class="clearfix">{$lines_array[3]}</div> <!-- 本文表示 -->

EOF;
//ブラウザ出力ここまで（HTML部は編集可）

for($i=0;$i<$photo_count;$i++){
  $caption = '';  
  if(!empty($caption_array[$i])){
	  $caption = $caption_array[$i];
  }
  foreach($extensionTypeList as $extensionVal){
	if(file_exists("{$img_updir}/{$id}_{$i}.{$extensionVal}")) {
  
//ブラウザ画像出力（HTML部は編集可）
echo <<<EOF
<div class="detailPhoto">
<p><a href="{$img_updir}/{$id}_{$i}.{$extensionVal}" title="{$caption}"><img src="{$img_updir}/{$id}_{$i}.{$extensionVal}" alt="{$caption}" width="270" /></a><br>{$caption}</p> <!-- キャプション表示 -->
</div>

EOF;
//ブラウザ画像出力ここまで（HTML部は編集可）
  break;
	}
  }
}
?>
<br />
<p class="back"><a href="javascript:history.back();">&laquo; 戻る</a></p>
<?php echo copyright_dsp($encodingType,$copyright); }//著作権表記削除不可?>
<!--▲▲ 記事表示部 ▲▲-->

</body>
</html>

</section>

<section>
 <h2>大手町Style整骨院 Facebookページ</h2>
 <div align="center"><a href="http://www.facebook.com/ootemati" target="_blank"><img src="img/s_fb.jpg" alt="大手町Style整骨院 Facebookページ" width="210" height="65" id="大手町Style整骨院 Facebookページ" /></a></div>
</section>

<section>
 <h2>お問い合わせにつきまして</h2>
 <div align="center"><a href="tel:082-569-8090"><img src="img/s_tel.jpg" alt="電話でのお問い合わせ" width="150" height="50" id="電話でのお問い合わせ" /></a>　<a href="mailto:ootemachisasuke@yahoo.co.jp"><img src="img/s_mail.jpg" alt="メールでのお問い合わせ" width="150" height="50" id="メールでのお問い合わせ" /></a></div>
</section>

<section>
<h2>SNSにて紹介する</h2>
<div class="ninja_onebutton">
<script type="text/javascript">
//<![CDATA[
(function(d){
if(typeof(window.NINJA_CO_JP_ONETAG_BUTTON_44c9d352ed68705c72f5087254a9bc5a)=='undefined'){
    document.write("<sc"+"ript type='text\/javascript' src='http:\/\/omt.shinobi.jp\/b\/44c9d352ed68705c72f5087254a9bc5a'><\/sc"+"ript>");
}else{ 
    window.NINJA_CO_JP_ONETAG_BUTTON_44c9d352ed68705c72f5087254a9bc5a.ONETAGButton_Load();}
})(document);
//]]>
</script><span class="ninja_onebutton_hidden" style="display:none;"></span><span style="display:none;" class="ninja_onebutton_hidden"></span>
</div>
</section>
</article>

<nav>
 <h2>サイトメニュー</h2>
  <ul>
   <li><a href="../smartphone/">ホーム</a></li>
   <li><a href="price.html">料金体系</a></li>
   <li><a href="access.html">アクセス</a></li>
   <li><a href="staff.html">スタッフ紹介</a></li>
   <li><a href="faq.html">よくある質問</a></li>
   <li><a href="voice.html">お客様の声</a></li>
   <li><a href="contact.html">お問い合わせ</a></li>
   <li><a href="pelvis.html">骨盤矯正</a></li>
   <li><a href="o_legs.html">O脚矯正</a></li>
   <li><a href="posture.html">猫背矯正</a></li>
   <li><a href="gaihan.html">外反母趾</a></li>
   <li><a href="trafficaccident.html">交通事故の治療</a></li>
   <li><a href="kogao.html">小顔・フェイシャル</a></li>
  </ul>
</nav>

<div id="page_back">
 <p><a href="#page_top">▲ページトップに戻る</a></p>
</div>

<footer>
 <p><a href="http://www.ootemachi-style.com/index.html?vmode=pc">PCサイトへ</a> ｜ <a href="sitemap.html">サイトマップ</a></p>
 <p class="copy">Copyright (C) 大手町Style整骨院 All Rights Reserved.</p>
</footer>

</body>
</html>