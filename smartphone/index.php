<!DOCTYPE HTML>
<html lang="ja">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5L3FD38');</script>
<!-- End Google Tag Manager -->
<meta charset="UTF-8">
<title>大手町Style整骨院</title>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="style.css">
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5L3FD38"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header>

<div id="page_top">
 <h1><a href="../"><img src="img/logo.png" alt="大手町Style整骨院" border="0" /></a></h1>
 <p style="text-align:center"><img src="img/main_1116.jpg" style="width:100%; max-width:950px;" alt=""></p>
</div>

</header>

<article>

<section>
 <h2>お知らせ・キャンペーン・ブログ更新情報</h2>
<dl>
<?php 
//----------------------------------------------------------------------
// 設定ファイルの読み込みとページ独自設定　※必要に応じて変更下さい(START)
//----------------------------------------------------------------------

//設定ファイルインクルード（相対パス）※設置箇所が変わる場合は変更して下さい
include_once("../photo_news/config.php");
//データファイル（news.dat）の相対パス ※設置箇所が変わる場合は変更して下さい
$file_path = '../photo_news/data/news.dat';
//記事詳細ページの相対パス（このファイルから見たnews_post.phpのパス）
$post_path = 'news_post.php';
//このページの文字コード。Shift-jisは「SJIS」、EUC-JPは「EUC-JP」と指定してください。デフォルトはUTF-8。
$encodingType = 'UTF-8';
//表示件数（このページのニュース表示数）
$news_dsp_count = 5;

//----------------------------------------------------------------------
// 設定ファイルの読み込みとページ独自設定　※必要に応じて変更下さい(END)
//----------------------------------------------------------------------

if(!$copyright){echo $warningMesse; exit;}else{
//ファイルの内容を取得　表示
$lines = newsListSortUser(file($file_path),$encodingType);
foreach($lines as $key => $val){
	if($key >= $news_dsp_count) break;
	  $lines_array = explode(",",$val);
	  $upymd = ymd2format($lines_array[1],$encodingType);//日付フォーマットの適用
	  $title = title_format($lines_array[3],$lines_array[2],$lines_array[0],$post_path);//タイトルのフォーマットの適用
	  //カテゴリの設定チェック（カテゴリが設定されていなかったら非表示処理）
	  $category = category_get($lines_array[4],$category_array,$encodingType);
	  
	  //NEWマーク表示処理　※タグ部変更可。画像でももちOK（さらに下にある「{$new_mark}」を移動すれば表示場所を変えられます）
	  if($new_mark = new_mark_func($lines_array[1],'<span style="color:red;font-size:12px;" class="new_mark"> NEW !</span>'));
		
//ブラウザ出力（HTML部は編集可）
echo <<<EOF
<dt class="cat_{$lines_array[4]}"> <!-- カテゴリ毎にclassを付与（カテゴリ毎にCSSで背景画像などを指定可能） -->
<span class="cat_name"{$category['hidden_style']}>{$category['name']}</span> <!--　カテゴリ名表示（削除してCSSで背景画像等で指定してもOK） -->
<span class="news_List_Ymd">{$upymd}{$new_mark}</span></dt>
<dd><span class="news_List_Title">{$title}</span></dd>


EOF;
}	
?>
<?php echo copyright_dsp($encodingType,$copyright); }//著作権表記削除不可?>
<div align="right"><a href="news_list.php">>>お知らせ・キャンペーン・ブログ一覧はこちら</a></div>
</dl>
</section>

<section>
 <h2>大手町Style整骨院 Facebookページ</h2>
 <div align="center"><a href="http://www.facebook.com/ootemati" target="_blank"><img src="img/s_fb.jpg" alt="大手町Style整骨院 Facebookページ" width="210" height="65" id="大手町Style整骨院 Facebookページ" /></a></div>
</section>

<section>
 <h2>お問い合わせにつきまして</h2>
 <div align="center"><a href="tel:082-569-8090"><img src="img/s_tel.jpg" alt="電話でのお問い合わせ" width="150" height="50" id="電話でのお問い合わせ" onclick="return gtag_report_conversion('tel:082-569-8090')" /></a>　<a href="mailto:ootemachisasuke@yahoo.co.jp"><img src="img/s_mail.jpg" alt="メールでのお問い合わせ" width="150" height="50" id="メールでのお問い合わせ" /></a></div>
</section>

<section>
<h2>SNSにて紹介する</h2>
<div class="ninja_onebutton">
<script type="text/javascript">
//<![CDATA[
(function(d){
if(typeof(window.NINJA_CO_JP_ONETAG_BUTTON_44c9d352ed68705c72f5087254a9bc5a)=='undefined'){
    document.write("<sc"+"ript type='text\/javascript' src='http:\/\/omt.shinobi.jp\/b\/44c9d352ed68705c72f5087254a9bc5a'><\/sc"+"ript>");
}else{ 
    window.NINJA_CO_JP_ONETAG_BUTTON_44c9d352ed68705c72f5087254a9bc5a.ONETAGButton_Load();}
})(document);
//]]>
</script><span class="ninja_onebutton_hidden" style="display:none;"></span><span style="display:none;" class="ninja_onebutton_hidden"></span>
</div>
</section>
</article>

<nav>
 <h2>サイトメニュー</h2>
  <ul>
   <li><a href="./">ホーム</a></li>
   <li><a href="price.html">料金体系</a></li>
   <li><a href="access.html">アクセス</a></li>
   <li><a href="staff.html">スタッフ紹介</a></li>
   <li><a href="faq.html">よくある質問</a></li>
   <li><a href="voice.html">お客様の声</a></li>
   <li><a href="contact.html">お問い合わせ</a></li>
   <li><a href="pelvis.html">骨盤矯正</a></li>
   <li><a href="o_legs.html">O脚矯正</a></li>
   <li><a href="posture.html">猫背矯正</a></li>
   <li><a href="gaihan.html">外反母趾</a></li>
   <li><a href="trafficaccident.html">交通事故の治療</a></li>
   <li><a href="kogao.html">小顔・フェイシャル</a></li>
  </ul>
</nav>

<div id="page_back">
 <p><a href="#page_top">▲ページトップに戻る</a></p>
</div>

<footer>
 <p><a href="http://www.ootemachi-style.com/index.html?vmode=pc">PCサイトへ</a> ｜ <a href="sitemap.html">サイトマップ</a></p>
 <p class="copy">Copyright (C) 大手町Style整骨院 All Rights Reserved.</p>
</footer>

</body>
</html>