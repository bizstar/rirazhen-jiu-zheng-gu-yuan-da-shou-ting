<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5L3FD38');</script>
<!-- End Google Tag Manager -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>お知らせ・キャンペーン・ブログ一覧｜広島の整骨院｜大手町Style整骨院</title>
<meta name="description" content="広島市中区にある大手町Style整骨院では、中国地方初の外反母趾研究所として骨盤矯正・O脚矯正・猫背矯正・交通事故治療・保険治療などをしています。" />
<meta name="keywords" content="広島整骨院,交通事故治療,骨盤矯正,O脚矯正,外反母趾,猫背矯正,保険治療" />
<meta name="author" content="大手町Style整骨院" />
<meta name="copyright" content="Copyright (c) 大手町Style整骨院" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<link type="text/css" rel="stylesheet" href="css/style.css" />
<script type="text/javascript" src="js/import.js"></script>
<meta name="robots" content="all">
<link rel="alternate"type="application/rss+xml"title="sitemap"href="sitemaps.xml"/>
<link rel="start" href="http://www.ootemachi-style.com"/>
<link rev="made" href="mailto:info@ootemachi-style.com"/>
<script type="text/javascript"> // iPhoneまたは、Androidの場合は振り分けを判断
 if (document.referrer.indexOf('http://ootemachi-style.com/') == -1 &&
 ((navigator.userAgent.indexOf('iPhone') > 0 &&
 navigator.userAgent.indexOf('iPad') == -1) ||
 navigator.userAgent.indexOf('iPod') > 0 ||
 navigator.userAgent.indexOf('Android') > 0)) 
{if(confirm('広島の整骨院｜大手町Style整骨院へようこそ。\nこのサイトにはスマートフォン用のページがあります。\n表示しますか？')) 
{location.href = 'http://ootemachi-style.com/smartphone';}}
</script>
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5L3FD38"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--head-->
<!--logo-tel-->
<div id="head" class="wrapper clearfix">
	<div id="head_Logo">
    <h1>中国地方初の広島市外反母趾研究所として骨盤矯正・O脚矯正・猫背矯正・交通事故治療・保険治療などを行っています。</h1>
    <div id="logo"><a href="../"><img src="images/logo.jpg" width="259" height="93" alt="" /></a></div>
    </div>
    <div id="headerContact">
        <div class="textR"><span>TEL:</span>082-569-8090</div>
        <p class="textR"><span>定休日/日曜日・祝日・木曜日</span></p>
        <p class="textR"><span>広島市中区大手町3-13-8</span></p>
        <p class="right"><a href="contact.html"><img src="images/contact_image.jpg" width="150" height="35" alt="お問い合わせ" /></a></p>
    </div>
</div>
<!--//logo-tel-->
<!--globalNavi-->
<div id="globalNavi">
<div class="wrapper">
   	<ul>
     	<li><a href="../">ホーム<p>HOME</p></a></li>
        <li><a href="price.html">料金体系<p>PRICE</p></a></li>
        <li><a href="access.html">アクセス<p>ACCESS</p></a></li>
        <li><a href="staff.html">スタッフ紹介<p>STAFF</p></a></li>
        <li><a href="faq.html">よくある質問<p>FAQ</p></a></li>
        <li><a href="voice.html">お客様の声<p>VOICE</p></a></li>
        <li class="last"><a href="contact.html">お問い合わせ<p>CONTACT</p></a></li>
    </ul>
</div>
</div>
<!--globalNavi-->
<!--//head-->

<div class="hImg">
	<div class="wrapper"><img src="images/main_1116.jpg" width="950" height="333" alt="" /></div>
</div>

<!--container-->
<div id="container" class="wrapper clearfix">
	<!--contents-->
    <div id="contents" class="left">
    
    	<!--Service-->
    	<div class="padT15 section clearfix">
                        
        </div>
        <!--//Service-->

            <!--見出し-->
            <div class="box">

<div id="main">
<h3>お知らせ・キャンペーン・ブログ一覧</h3>
<dl class="new">
<?php
//----------------------------------------------------------------------
// 　任意設定箇所 (STSRT)　設置箇所によっては必要に応じて変更下さい
//----------------------------------------------------------------------

//設定ファイルインクルード（相対パス）※設置箇所が変わる場合は変更して下さい
include_once("photo_news/config.php");
//データファイル（news.dat）の相対パス ※設置箇所が変わる場合は変更して下さい
$file_path = 'photo_news/data/news.dat';
//記事詳細ページの相対パス（このファイルから見たnews_post.phpのパス）
$post_path = 'news_post';
//記事詳細にポップアップを使用する場合の相対パス（設定ファイルにて設定可）
if($popup == 1)$post_path = 'photo_news/popup.php';

//このページの文字コード。Shift-jisは「SJIS」、EUC-JPは「EUC-JP」と指定。デフォルトはUTF-8。
$encodingType = 'UTF-8';

//----------------------------------------------------------------------
// 　任意設定箇所 (END)　設置箇所によっては必要に応じて変更下さい
//----------------------------------------------------------------------

//ファイルの内容を取得　表示
$lines = newsListSortUser(file($file_path),$encodingType);
$pager_res = pager_dsp($lines,$pagelength,$pagerDispLength,$encodingType);//ページャー生成
$pager_dsp = '<div class="pager_link">'.$pager_res['dsp'].'</div>';//ページャー用html
?>
<?php echo $pager_dsp;//ページャー表示?>
<ul id="news_list">
<?php
if(!$copyright){echo $warningMesse; exit;}else{
for($i = $pager_res['index']; ($i-$pager_res['index']) < $pagelength; $i++){
	if(!empty($lines[$i]) && strpos($lines[$i], 'no_disp')===false){
	$lines_array = explode(",",$lines[$i]);
	$upymd = ymd2format($lines_array[1],$encodingType);//日付フォーマットの適用
	$title = title_format($lines_array[3],$lines_array[2],$lines_array[0],$post_path);//タイトルのフォーマット
	
	//カテゴリの設定チェック（カテゴリが設定されていなかったら非表示処理）
	$category = category_get($lines_array[4],$category_array,$encodingType);
	
	//NEWマーク表示処理　※タグ部変更可。画像でももちOK（さらに下にある「{$new_mark}」を移動すれば表示場所を変えられます）
	if($new_mark = new_mark_func($lines_array[1],'<span style="color:red;font-size:12px;" class="new_mark"> NEW !</span>'));
		
//ブラウザ出力（編集可）
echo <<<EOF

<dt><li class="cat_{$lines_array[4]}"> <!-- カテゴリ毎にclassを付与（カテゴリ毎にCSSで背景画像などを指定可能） -->
<span class="cat_name"{$category['hidden_style']}>{$category['name']}</span> <!--　カテゴリ名表示（削除してCSSで背景画像等で指定してもOK） -->
<span class="news_List_Ymd">{$upymd}</span></dt>  <!-- 日付 -->
<dd><span class="news_List_Title">{$title}</span>{$new_mark} </dd><!-- タイトル、「NEW」マーク表示 -->
</li> 

EOF;
	}
}
?>
<?php echo $pager_dsp;//ページャー表示?>
<?php echo copyright_dsp($encodingType,$copyright); }//著作権表記削除不可?>
</dl>

 </div>
   </div>
             <div class="box">
				<div align="center"><img src="images/contact_banner.jpg" width="717" height="230" border="0" usemap="#contact_banner" />
<map name="contact_banner">
  <area href="contact.html" shape="rect" coords="403,169 695,212" />
  <area shape="default" nohref="nohref" />
</map></div>
                    </div>
            <div class="right"><a href="#head"><img src="images/pagetop.gif" width="86" height="19" alt="" /></a></div>
            <!--あいさつ文-->
            
        </div>

    <!--//contents-->
    
    <!--right-->
    <div id="right" class="padT15">
    	<div class="subBox">
            <div class="subContactBox">
                <div><img src="images/sub_menu_top.gif" width="223" height="33" alt="" /></div>
                <ul>
                    <li><a href="../">HOME</a></li>
                    <li><a href="pelvis.html">骨盤矯正</a></li>
                    <li><a href="o_legs.html">O脚矯正</a></li>
                    <li><a href="posture.html">猫背矯正</a></li>
                    <li><a href="gaihan.html">外反母趾</a></li>
                    <li><a href="trafficaccident.html">交通事故の治療</a></li>
                    <li><a href="kogao.html">小顔・フェイシャル</a></li>
                </ul>
                <div><img src="images/sub_menu_bottom.gif" width="223" height="12" alt="" /></div>
           </div>
     	</div>
               <p><img src="images/ootemachi.jpg" width="223" height="210" alt="大手町Style整骨院" /></p><br />
               <p><a href="http://www.facebook.com/ootemati" target="_blank"><img src="images/fb_image.jpg" width="223" height="54" alt="Facebookページ" /></a></p>
    </div>
    <!--//right-->
</div>
<!--//container-->
</body>
<!--footer-->
<div id="footer">
<div id="footerbox" class="clearfix">
  <div id="footerNavi">
  <p><a href="../">HOME</a>&nbsp;｜&nbsp;<a href="price.html">料金体系</a>&nbsp;｜&nbsp;<a href="access.html">アクセス</a>&nbsp;｜&nbsp;<a href="staff.html">スタッフ紹介</a>&nbsp;｜&nbsp;<a href="faq.html">よくある質問</a>&nbsp;｜&nbsp;<a href="voice.html">お客様の声</a>&nbsp;｜&nbsp;<a href="contact.html">お問い合わせ</a></p>
  <p><a href="pelvis.html">骨盤矯正</a>&nbsp;｜&nbsp;<a href="o_legs.html">O脚矯正</a>&nbsp;｜&nbsp;<a href="posture.html">猫背矯正</a>&nbsp;｜&nbsp;<a href="gaihan.html">外反母趾</a>&nbsp;｜&nbsp;<a href="trafficaccident.html">交通事故の治療</a>&nbsp;｜&nbsp;<a href="privacy.html">個人情報保護法</a>&nbsp;｜&nbsp;<a href="sitemap.html">サイトマップ</a></p>
  <address>copyright &copy; 2013  大手町Style整骨院. All rights reserved.&nbsp;</address>
  </div>
  <div id="footerLogo"><a href="index.html"><img src="images/footer_logo.png" width="178" height="66" alt="" /></a></div>
</div>
</div>
<!--//footer-->
</html>