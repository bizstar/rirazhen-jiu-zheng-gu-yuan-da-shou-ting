<?php
#######################################################################################
##
#  PHP新着情報、お知らせプログラム News03（画像アップ・エディタ機能搭載版）ver1.0.0 (2013.09.7公開)
#
#  フリー版のNews02ニュースプログラムにページング付き一覧ページ、カテゴリ、
#　画像キャプション設定、詳細ページをなどの機能を追加したバージョンです。
#　WPと同じような感覚です。WPよりも設置が容易、管理画面がシンプルなどWPより使いやすのが特徴です。
#
#  トップーページの新着情報やお知らせなどに適しています。
#　インラインフレームでも良いですが、トップページに直接埋め込むことでSEOにも効果的です。
#  改造や改変は自己責任で行ってください。
#	
#  今のところ特に問題点はありませんが、不具合等がありましたら下記までご連絡ください。
#  MailAddress: info@php-factory.net
#  name: k.numata
#  HP: http://www.php-factory.net/
##
#######################################################################################
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>新着情報、お知らせ</title>
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="Keywords" content="" />
<meta name="Description" content="" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<link href="style.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body id="news_index">
<?php
include_once("config.php");//設定ファイルインクルード

//----------------------------------------------------------------------
// 　任意設定箇所 (STSRT)　設置箇所によっては必要に応じて変更下さい
//----------------------------------------------------------------------

//記事詳細ページの相対パス（このファイルから見たnews_post.phpのパス）
$post_path = '../news_post.php';
//記事詳細にポップアップを使用する場合（設定ファイルにて設定可）
if($popup == 1)$post_path = 'popup.php';
//----------------------------------------------------------------------
// 　任意設定箇所 (END)　設置箇所によっては必要に応じて変更下さい
//----------------------------------------------------------------------

if(!$copyright){echo $warningMesse; exit;}else{?>
<div id="news_wrap">
<ul id="news_list">
<?php
//ファイルの内容を取得　表示
$lines = newsListSortUser(file($file_path));
foreach($lines as $key => $val){
	if($key >= $news_dsp_count) break;
	$lines_array = explode(",",$val);
	$upymd = ymd2format($lines_array[1]);//日付フォーマットの適用
	$title = title_format($lines_array[3],$lines_array[2],$lines_array[0],$post_path);//タイトルのフォーマットの適用
	//カテゴリの設定チェック（カテゴリが設定されていなかったら非表示処理）
	$category = category_get($lines_array[4],$category_array);
	
	//NEWマーク表示処理　※タグ部変更可。画像でももちOK（さらに下にある「{$new_mark}」を移動すれば表示場所を変えられます）
	if($new_mark = new_mark_func($lines_array[1],'<span style="color:red;font-size:12px;" class="new_mark"> NEW !</span>'));
		
//ブラウザ出力	
echo <<<EOF
<li class="cat_{$lines_array[4]}"> <!-- カテゴリ毎にclassを付与（カテゴリ毎にCSSで背景画像などを指定可能） -->
<span class="cat_name"{$category['hidden_style']}>{$category['name']}</span> <!--　カテゴリ名表示（削除してCSSで背景画像等で指定してもOK） -->
<span class="news_List_Ymd">{$upymd}</span>  <!-- 日付 -->
<span class="news_List_Title">{$title}</span>{$new_mark} <!-- タイトル、「NEW」マーク表示 -->
</li>
EOF;
}	
?>
</ul>
<p class="to_list"><a href="../news_list.php" target="_parent">一覧へ</a></p>
<?php echo $copyright; }//著作権表記削除不可?>
</div>
</body>
</html>