<?php
//設定ファイルインクルード
include_once("config.php");
if(!$copyright){echo $warningMesse; exit;}else{
	if(!empty($_GET['id'])){
		$id=$_GET['id'];
	}else{
		exit('パラメータがありません');	
	}
    $lines = file($file_path);
	foreach($lines as $key => $val){
	$lines_array = explode(",",$val);
	  if($lines_array[0] == $id){
		  $end_flag = 1;
		  break;
	  }
	}
	if($end_flag != 1) exit('データ取得エラー');
$lines_array[5] = rtrim($lines_array[5]);
$lines_array[1] = ymd2format($lines_array[1]);//日付フォーマットの適用
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php	echo $lines_array[2];?></title>
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="style.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>   
<script type="text/javascript">
 $(document).ready( function () {
 $("#detailWrap a").attr("target","_blank");//本文内のaタグに_blankを付与 ※変更可
})
</script>
</head>
<body id="news_popup">
<?php
if(!empty($lines_array[5])){
	$caption_array = explode('<>',$lines_array[5]);
}
//ブラウザ出力（HTML部は編集可）
echo <<<EOF
<!--<p style="color:red;font-size:13px;text-align:center;margin:0 0 15px;">このページは投稿内容確認用です。実際の表示はサイトにてご確認ください</p>-->
<h2>{$lines_array[2]}</h2>
<p class="up_ymd">{$lines_array[1]}</p>
<div id="detailWrap">{$lines_array[3]}</div>

EOF;
if(strpos($id,'no_disp')!==false) $id = str_replace('no_disp','',$id);
  for($i=0;$i<$photo_count;$i++){
	$caption = '';  
	if(!empty($caption_array[$i])){
		$caption = $caption_array[$i];
	}
	foreach($extensionTypeList as $extensionVal){
	  if(file_exists("{$img_updir}/{$id}_{$i}.{$extensionVal}")) {
//ブラウザ出力（HTML部は編集可）
echo <<<EOF
<p id="photoNo{$i}" class="detailPhoto" align="center"><img src="{$img_updir}/{$id}_{$i}.{$extensionVal}" alt="{$caption}" /></p>
<p class="photo_caption" align="center">{$caption}</p><br />

EOF;
break;
	  }
	}
  }
?>
<br />
<p class="close_btn"><a href="javascript:window.close();">CLOSE</a></p>
<?php echo $copyright;}//著作権表記無断削除不可?>
</body>
</html>